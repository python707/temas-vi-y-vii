# Modulo que proporciona la funcionalidad de una pila de objetos
# Proporciona Excepciones y la clase Pila.

#-----------------------------------------------
class PilaVacia(Exception):
    def __init__(self):
        self.value='Error:Pila Vacia!!'
#-----------------------------------------------
class PilaLlena(Exception):
    def __init__(self):
        self.value='Error: Pila Llena!!'

#-----------------------------------------------
class Pila:
    n=None
    items=None
    sp=None
    '''------------------------------------------'''
    def __init__(self,n=10):
        self.items=[]
        self.sp=-1
        self.n=n
    '''------------------------------------------'''
    def push(self, item):
        if not self.isFull():
            self.items.append(item)
            self.sp+=1
        else:
            raise PilaLlena()
    '''------------------------------------------'''
    def pop(self):
        if not self.isEmpty():
            item=self.items[self.sp]
            self.items.reverse()
            self.items.remove(item)
            self.items.reverse()
            self.sp-=1
        else:
            raise PilaVacia()
    '''------------------------------------------'''
    def isEmpty(self):
        return self.sp==-1
    
    def isFull(self):
        return self.sp==(self.n-1)
