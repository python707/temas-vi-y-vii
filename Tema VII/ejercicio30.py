# Programa que amplia la implementación de métodos de la clase object
#

class Cordenada(object):
        
    def __init__(self,x=0,y=0):
        self.x=x
        self.y=y
            
    def __str__(self):
            return "{'x':"+str(self.x)+"'y':"+str(self.y)+"}"


    def __eq__(self,c2):
        return self.x==c2.x and self.y==c2.y

    

    def __lt__(self,c2):
        return self.x<c2.x or self.y<c2.y

