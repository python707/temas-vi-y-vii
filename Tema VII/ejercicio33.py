# Programa que muestra como resolver el problema de la invocación a los métodos
# nombrados igual en las clases base.

class Base1:
    def __init__(self,a):
        self.a=a
    def aumenta(self,i):
        self.a+=i
    def __str__(self):
        return str(self.a)
    
    


class Base2:
    def __init__(self,b):
        self.b=b
    def aumenta(self,i):
        self.b*=i
    def __str__(self):
        return str(self.b)


class Ejemplo(Base1,Base2):
    def __init__(self,a,b,c):
        Base1.__init__(self,a)
        Base2.__init__(self,b)
        self.c=c
        
    def aumenta(self,i):
        Base1.aumenta(self,i)
        Base2.aumenta(self,i)
        self.c=self.c**i
        
    def __str__(self):
        return '('+str(Base1.__str__(self))+','+str(Base2.__str__(self))+','+str(self.c)+')'
